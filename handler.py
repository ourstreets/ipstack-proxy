import os
import json
import urllib.request


def proxy(event, context):
    if event["path"] == "/check":
        path = f"/{event['requestContext']['identity']['sourceIp']}"
    else:
        path = event["path"]
    response = urllib.request.urlopen(
        f'http://api.ipstack.com{path}?access_key={os.environ["API_KEY"]}&format=1'
    )

    return {
        "statusCode": response.getcode(),
        "body": response.read().decode(),
        "headers": {"Access-Control-Allow-Origin": os.environ.get("CORS_ORIGIN", "*")},
    }
